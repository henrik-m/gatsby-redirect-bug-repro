import * as React from "react"
import { Router } from "@reach/router"
import { Link } from "gatsby"

const S1 = () => <div>Settings 1</div>
const S2 = () => <div>Settings 2</div>
const S3 = () => <div>Settings 3</div>
const Default = () => <div>Default</div>

const SettingsPage = (props) => {
  console.log(props)
  const path = props.uri
  return (
    <div>
      <h1>Settings</h1>
      <div>These are settings:</div>
      <ul>
        <li><Link to={`${path}/s1`}>Go to Settings 1</Link></li>
        <li><Link to={`${path}/s2`}>Go to Settings 2</Link></li>
        <li><Link to={`${path}/s3`}>Go to Settings 3</Link></li>
      </ul>
      <Router basepath={`${path}`}>
        <S1 path="/s1" />
        <S2 path="/s2" />
        <S3 path="/s3" />
        <Default path="/" />
      </Router>
    </div>
  )
}

export const Head = () => <title>Settings</title>

export default SettingsPage
