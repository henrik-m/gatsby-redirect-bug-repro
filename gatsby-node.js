const _ = require("lodash")

// list of pages that support clientside routing
const clientSideRoutes = ["settings"]

exports.createPages = async ({ graphql, actions }) => {
  const { createPage, createRedirect } = actions

  const templates = await graphql(`
    query GetTemplates {
      allFile(filter: { sourceInstanceName: { eq: "templates" } }) {
        nodes {
          name
          relativeDirectory
          absolutePath
        }
      }
    }
  `)

  const templateNodes = templates.data.allFile.nodes

  // Put the index page at the end so the redirect is created last and does not overrule other redirects
  const topLevelIndexPage = _.remove(
    templateNodes,
    node => node.name === "index"
  )[0]
  templateNodes.push(topLevelIndexPage)

  templateNodes.forEach(template => {
    const slug = template.name === "index" ? "/en/" : `/en/${template.name}/`
    let matchPath = slug

    // create redirect for client side routing
    if (clientSideRoutes.includes(template.name)) {
      matchPath = slug + "*"

      createRedirect({
        fromPath: matchPath,
        toPath: slug,
        statusCode: 200,
      })
    }

    createPage({
      path: slug,
      matchPath,
      component: template.absolutePath,
      context: {
        name: template.name,
        language: "en",
      },
    })
  })
}
